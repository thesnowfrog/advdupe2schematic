import os
import json

def readbin(filename):
    with open(filename,'rb') as file:
        content = file.read()
    return content

def readfile(filename):
    with open(filename,'r') as file:
        content = file.read()
    return content

def writebin(content, filename):
    with open(filename,'wb') as file:
        file.write(content)

def writefile(content,filename):
    with open(filename,'w') as file:
        file.write(content)


class WireDevice():
    def __init__(self,entid,obj):
        self.e2name = None
        self.e2code = None
        self.inputs = []
        self.outputs = []
        self.outwire = {}
        self.entid = entid
        self.entclass = obj['Class']
        self.wired = False
        self.others = {}
        self.acf = {}

        if '_inputs' in obj:
            for i in range(len(obj['_inputs'][0])):
                self.inputs.append('{}:{}'.format(obj['_inputs'][0][i],obj['_inputs'][1][i].lower()))
                self.inputs.sort()
                
        if '_outputs' in obj:
            for i in range(len(obj['_outputs'][0])):
                self.outputs.append('{}:{}'.format(obj['_outputs'][0][i],obj['_outputs'][1][i].lower()))
                self.outputs.sort()

        if obj['Class'] == 'acf_gun':
            self.acf['GunID'] = obj['Id']
            
        if obj['Class'] == 'acf_engine':
            self.acf['EngineID'] = obj['Id']
            
        if obj['Class'] == 'acf_gearbox':
            self.acf['GearboxID'] = obj['Id']
            self.acf['Gear0'] = obj['Gear0']
            self.acf['Gear1'] = obj['Gear1']
            self.acf['Gear2'] = obj['Gear2']
            self.acf['Gear3'] = obj['Gear3']
            self.acf['Gear4'] = obj['Gear4']
            self.acf['Gear5'] = obj['Gear5']
            self.acf['Gear6'] = obj['Gear6']
            self.acf['Gear7'] = obj['Gear7']
            self.acf['Gear8'] = obj['Gear8']
            self.acf['Gear9'] = obj['Gear9']
            
        if obj['Class'] == 'acf_fueltank':
            self.acf['FuelID'] = obj['Id']

        if obj['Class'] == 'acf_ammo':
            self.acf['BoxID'] = obj['Id']
            self.acf['AmmoID'] = obj['RoundId']
            self.acf['RoundType'] = obj['RoundType']
            self.acf['RoundProjectile'] = obj['RoundProjectile']
            self.acf['RoundPropellant'] = obj['RoundPropellant']
            #self.acf['RoundData1'] = obj['RoundData1']
            #self.acf['RoundData2'] = obj['RoundData2']
            #self.acf['RoundData3'] = obj['RoundData3']
            #self.acf['RoundData4'] = obj['RoundData4']
            self.acf['RoundData5'] = obj['RoundData5']
            self.acf['RoundData6'] = obj['RoundData6']
            self.acf['RoundData7'] = obj['RoundData7']
            self.acf['RoundData8'] = obj['RoundData8']
            self.acf['RoundData9'] = obj['RoundData9']

        if 'EntityMods' in obj:
            
            if 'WheelLink' in obj['EntityMods']:
                self.outwire['WheelLink'] = obj['EntityMods']['WheelLink']['entities']
                self.wired = True
                
            if 'FuelLink' in obj['EntityMods']:
                self.outwire['FuelLink'] = obj['EntityMods']['FuelLink']['entities']
                self.wired = True
                
            if 'GearLink' in obj['EntityMods']:
                self.outwire['GearLink'] = obj['EntityMods']['GearLink']['entities']
                self.wired = True
                
            if 'WireDupeInfo' in obj['EntityMods']:
                if 'Wires' in obj['EntityMods']['WireDupeInfo']:
                    
                    wiring = obj['EntityMods']['WireDupeInfo']['Wires']
                    for i in wiring:
                        self.outwire[i] = [str(int(wiring[i]['Src'])), str(wiring[i]['SrcId'])]
                    if len(wiring)>0:
                        self.wired = True

            if 'ACFAmmoLink' in obj['EntityMods']:
                self.outwire['AmmoLink'] = obj['EntityMods']['ACFAmmoLink']['entities']
                self.wired = True
                        
            if 'acfsettings' in obj['EntityMods']:
                for i in obj['EntityMods']['acfsettings']:
                    self.acf[i] = obj['EntityMods']['acfsettings'][i]
                

        if '_name' in obj:
            self.e2name = obj['_name']
            self.e2code = obj['_original'].replace('€','\n').replace('£','"')

        if 'action' in obj:
            self.others['action'] = obj['action']

        if 'value' in obj:
            self.others['value'] = []
            for i in obj['value']:
                self.others['value'].append( [i["Value"], i['DataType'].lower()] )

class Contraption():
    def __init__(self):
        self.chips = {}
        self.props = {}
        self.name = ''

    def loadjson(self,filename):
        dupe = LoadDupeJson(filename)
        self.name = filename.split('.')[0]
        ents = dupe['Entities']
        for entid in ents:
            obj = ents[entid]
            if "EntityMods" in obj:
                if "WireDupeInfo" in obj['EntityMods']:
                    self.chips[str(int(entid))] = WireDevice(str(int(entid)),obj)
        print(self.name,'loaded')
        

    def chipinfo(self,chipid):
        
        print(chipid, self.chips[chipid].entclass)
        
        if self.chips[chipid].e2name:
            print(self.chips[chipid].e2name)
            
        if len(self.chips[chipid].inputs)>0:
            print('In')
            for j in self.chips[chipid].inputs:
                print('    ',j)
                
        if len(self.chips[chipid].outputs)>0:
            print('Out')
            for j in self.chips[chipid].outputs:
                print('    ',j)

        if self.chips[chipid].wired:
            print('Wiring')
            for j in self.chips[chipid].outwire:
                if len(self.chips[chipid].outwire[j])>0 and j not in ['WheelLink','AmmoLink','GearLink','FuelLink']:
                    wiredid = self.chips[chipid].outwire[j][0]
                    src = self.chips[chipid].outwire[j][1]
                    try:
                        wiredclass = self.chips[wiredid].entclass
                    except KeyError:
                        wiredclass = 'prop'
                    if wiredclass == 'gmod_wire_expression2':
                        print('    {} ---> {} <[{} "{}" {}]'.format(j, src, wiredid, self.chips[wiredid].e2name, wiredclass))
                    else:
                        print('    {} ---> {} <[{} {}]'.format(j, src, wiredid, wiredclass))
                    
            if 'WheelLink' in self.chips[chipid].outwire:
                for i in self.chips[chipid].outwire['WheelLink']:
                    print('    {} ---> [{}]'.format(j, i))
                    
            if 'AmmoLink' in self.chips[chipid].outwire:
                for i in self.chips[chipid].outwire['AmmoLink']:
                    print('    {} ---> [{}]'.format(j, i))

        if len(self.chips[chipid].acf)>0:
            print('ACF Settings')
            for j in self.chips[chipid].acf:
                print('    {}: {}'.format(j, self.chips[chipid].acf[j]))
            

        if self.chips[chipid].e2code and False: ### disabled
            print('Code')
            print(self.chips[chipid].e2code)

        if len(self.chips[chipid].others)>0:
            if 'value' in self.chips[chipid].others:
                print('Values')
                c=1
                for i in self.chips[chipid].others['value']:
                    print('    ',c, i)
                    c+=1

            if 'action' in self.chips[chipid].others:
                print('Action:',self.chips[chipid].others['action'])
                
        print()

    def listchips(self):
        for i in self.chips:
            self.chipinfo(i)
        
    def wirechip(self, from_id, from_n, to_id, to_n):
        if from_n.find(':')>0:
            from_type = from_n.split(':')[1]
        else:
            from_type = 'int'
        if from_type == 'wirelink':
            self.chips[from_id].outwire[from_n] = [to_id, '<entity>']
        else:
            if to_n.find(':')>0:
                to_type = to_n.split(':')[1]
            else:
                to_type = 'int'
            if from_type == to_type:
                self.chips[from_id].outwire[from_n] = [to_id, to_n]
            else:
                print('Error: incompatible types')

    def unwire(self, from_id, from_n):
        self.chips[from_id].outwire[from_n] = []


def LoadDupeJson(filename):
    with open(filename,'r', encoding='windows-1252') as file:
        a = file.read()
        obj = json.loads(a)
        return obj
    
e2path = "/home/anon/.local/share/Steam/steamapps/common/GarrysMod/garrysmod/data/expression2/"
advpath = "/home/anon/.local/share/Steam/steamapps/common/GarrysMod/garrysmod/data/advdupe2/"

adv2 = Contraption()
adv2.loadjson('testdupe.json')
adv2.listchips()

